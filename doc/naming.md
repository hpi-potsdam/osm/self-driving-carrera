# Naming
The various revisions are named in ascending order after aircraft / flight objects / aircraft manufacturers. Starting at A, over B, then C and so on ...
This currently results in the following designations for the different versions:
A (ntonov)
B (oeing)
C (oncorde)