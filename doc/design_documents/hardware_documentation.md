# Hardware Design

Note: Unless otherwise noted, all component numbers refer to revision 2.

For access to the original hardware design project in EasyEDA, please email
leon.matthes@student.hpi.de or bjoern.daase@student.hpi.de

## General Design & Layout

The self driving slot car PCB is divided into 3 major sections:
1. Power Delivery
    - Splits the ~13-14V potential on the track into sections for the motor, logic and track input.
    - Stores some energy in the C8 Capacitor as a buffer for the logic circuitry
    - Converts the energy allocated for logic into 3.3V for the ESP32
2. Motor/Driving
    - The motor should use the highest voltage available
    - No voltage conversion takes place
    - Power to the motor must be controllable from the ESP32
3. Logic
    - ESP32-PICO is the main logic processor
    - Chosen for ease of integration and rich feature set
    - MPU6050 as accelerometer/Gyro
    - Chosen for ease of integration, extensive feature set & Arduino driver support

### Power Delivery
The first job of the power delivery is to ensure reverse-polarity protection.

In Rev.1 this was still done with a full bridge rectifier.
This includes double the voltage drop of a normal diode though, therefore it was switched
in Rev.2 to just use the diodes used to separate the two power domains.

This separation of power domains is the second job of the power delivery.
Interference, voltage spikes/drops, etc. from the motor should never fall onto the power input
for the logic board, as that would likely make the voltage too unstable for the ESP32.
A third small power domain is created for the Power indicator LED and track signal.
This is achieved with the diodes D1, D2, D3.

The power for the ESP32 is "buffered" in the capacitor C8 so the ESP32 has some power available
when driving over a lane change segment, where there is no power input available for a few milliseconds.
C8 is placed in front of the Linear voltage regulator (LDO) U3 that converts the input voltage to 3.3V.
Because the capacitor is placed before the LDO, the LDO can assure a stable 3.3V output even when the capacitor
voltage starts to drop.
This is the third job of the power delivery.

The voltage divider connected to ADC reduces the voltage to a level that is compatible with the ESP32 so
it can use an analog read to calculate how much energy is left.
This might be used in the future to initiate power saving measurements, should the input voltage drop too low.

### Logic circuitry
The main microcontroller is a ESP32-PICO-D4 (ESP32-PICO-V3 in Rev.3).
It's main layout is inspired by the [Watchy](https://watchy.sqfmi.com/docs/hardware) open source smartwatch.

For the antenna we chose a ceramic antenna, because of it's small PCB footprint and (hopefully) better performance.

The ESP32 is connected to a MPU6050 accelerometer/gyroscope combo unit via I2C.
We chose this unit because it was already available on prior dev boards, offers good driver support and has good-enough performance.
It's main layout originates from it's datasheet.

For improved performance, the I2C bus is pulled-up with 2.4K Ohm resistors.

The ESP32 has access to a status led (SLED) and a button that can be used freely for debugging, or other purposes.

It also has access to an infrared LED that can be used to broadcast the car ID to the lane switch segments.
In combination with a pressed controller of the same ID the lane switch should activate, though this is currently untested.

#### ESP32 programming
Initial programming of the ESP32 must be done via UART with an external USB to UART bridge.
For this connect the RX and TX pins to the UART bridge.
Remember to reverse it, TX of the UART bridge must be RX on the ESP and vice versa.
Also remember to use a 3.3V UART bridge, NOT a 5V one!

Connect the ground planes of the UART bridge and the ESP32 and connect voltage input to:
1. \> 3.3V - connect it to VIN, anything that the LDO can handle is fine (i.e. 5V).
2. 3.3V - connect directly to 3.3V pin.

To enter the bootloader, the IO0 pin of the ESP must be pulled to ground, then the ESP must be reset (use the RST button) and then the IO0 pin should
be left floating.
You should now have entered the bootloader and should be able to flash a program.
For further programming, we recommend using WiFi.

### Motor control
To control the motor, we initially (Rev. 1) used an H-Bridge.
This allowed us to drive the motor backwards, as well as forward.
The problem was that the H-Bridge could only take about 1A of current.
Because our motor has an internal resistance of only 5 Ohm, the inrush current
of the motor is up to 3A and the H-Bridges regularly blew up...

Furthermore, the car can't drive backwards anyway, as that blocks the sliding contacts, so that feature is not useful anyways.

The solution, implemented in Rev. 2, is to use a half-bridge that can either:
- provide full power to the motor
- Suck the energy out of the motor to brake rapidly

It is important to NOT activate the brake together with the motor power, as that would create a short circuit over the high-power rail.
Diode D4 should in theory prevent this on a hardware level. We have however not tested this and recommend adding a short delay in software
when switching between braking and driving.

To gradually control the speed of the motor, use PWM.

### Known issues & Rev. 3
The current boards are from Revision 2 and feature a few minor issues as well as some major possibilities of improvement.

A revision 3 is currently in an almost-ready state.
It is considerably more risky than the switch from Revision 1 to 2, but should fix most of the remaining issues and generally improve the board.

#### Jump-start on power-on
The MTR pin is connected to a pin that is pulled high on power-on.
This causes the motor to receive power when the chip is either reset or powered on for the first time.
Because the car can accelerate extremely quickly this usually causes it to jump forward and derail at the next corner.

The easiest workaround is to place the car on the track so the rear wheels can spin freely when the power is connected, then
wait until the chip is booted and power to the motor is turned off.
Never block the rear wheels from spinning when doing this!
The bug luckily does not appear when using a software reset, e.g. when flashing code over WiFi.

This bug is not yet fixed in Revision 3!

#### Analog read on TRK pin
Due to the voltage levels on the TRK pin produced by the voltage divider, the TRK pin must always be read with an analog read.
The problem with this is that the sample rate is much lower. Almost too low to read the signal produced by the Carrera controller.
In Revision 3 this is moved to a pull-down mechanism, so the signal on the TRK pin is either high or low, always inverse to the input signal.

#### Inrush current of C8
Because C8 is such a large capacitor, it can produce considerable inrush current.
This technically always overloads the diode in front of the capacitor.

Therefore revision 3 features a large 51 Ohm current limit resistor.
The diodes could then be replaced by Shottky diodes that feature a lower voltage drop, even if they can only handle lower currents.

#### Wrong antenna frequency
The antenna chosen for Revision 1 actually is a 2.6GHz frequency antenna, instead of the 2.45GHz that WiFi uses.
Because it still worked well, we chose to keep it for Revision 2.
In Revision 3 it was changed to the correct 2.45GHz.

#### Wasted power in LDO
A linear voltage regulator, whilst easy to implement is very inefficient, as it wastes all the excess power as heat.
This means our LDO with an input of 12V and output of 3.3V has only a 27% efficiency!

In Revision 3 this was changed to a switching voltage regulator (DC-DC converter) from Texas Instruments that has much higher efficiency.
The specific chip from TI was chosen due to it's extensive datasheet and very small output inductor requirement.
There is also a version of the same chip with fixed 3.3V output, as well as one with smaller output current (beneficial due to smaller
output voltage ripple) which would simplify the board layout, but these are not available on JLCPCB and/or more expensive.

In theory the switch to a DC-DC converter should provide a sufficient 3.3V supply at an efficiency of >80%, requiring a much smaller
C8 capacitor, or allowing the ESP to consume more power for longer.

#### Car Lights
Revision 3 also integrates the ability to control up to 3 lights using the L+/- XL+/- and B+/- pads.

#### ESP-PICO-V3
Espressif has recently released the new, and slightly improved ESP-PICO-V3.
It is more or less a drop-in replacement, expect for some ports which have switched around.

The PICO V3 is already placed in Revision 3, though not all ports have been checked yet.
BTN for example still needs to be moved.

#### Board dimensions
The board from Revision 2 can only fit in one or two of the cars available to the OSM group.
Revision 3 features a layout that should be more compatible and fit in around half the cars available.


### Notes and considerations
#### Power traces
The traces to the motor are only wide enough for 2A of sustained current.
As noted earlier, the motor can pull up to 3A.
Due to their very large width, it was not practical to widen the traces to 3A.
This is not a problem, as long as the wheels are not blocked from spinning!

The traces can easily handle a spike of 3A, just not for a sustained amount of time.

#### Resistor sizes
With such small resistors, most of the resistors chosen operate at the limit of how much energy they can dissipate.
When changing the resistor values, make sure to both make sure you're not exceeding the maximum current and wattage
allowed for these resistors.

#### Component types
JLCPCB does not just charge per component, but after a certain number of different component types, each new type of component costs an additional fee.
Therefore, reducing the different types of capacitors, resistors, etc. can safe quite a good amount of money.

In revision 3, most resistors that could have been replaced with similar ones have already been replaced.
Therefore, some resistors might seem suboptimal for their use case, but they should still work well enough.

#### Further reading

IR LED
http://redlichelectronics.de/ir-diode.html

Another DIY Decoder (Template for our motor control)
http://redlichelectronics.de/decoder_3G.html

Carrera track protocol
http://slotbaer.de/carrera-digital-124-132/9-cu-daten-protokoll.html
