\section{Case Study - Self-Driving Slot Car Debugging}
\label{sec:Case_Study}

Previous research has shown that there can be considerable gaps between theoretical and experimental results for transmission communication media~\cite{DBLP:conf/etfa/SenoVT11}.
Especially the power consumption of our custom \gls{pcb} in interplay with the environment-dependent power consumption of wireless protocols is expected to be difficult to simulate representatively.
Hence, we abstained from simulations and conducted a hard- and software implementation directly.

This section first describes our experiment setup using slot cars in \Cref{subsec:Experiment_setup} and explicitly highlights the energy consumption constraints of our experiment in \Cref{subsec:Energy_Consumption}.
Then, we apply the techniques described in \Cref{sec:Debugging_Techniques_Comparison} to our slot car in \Cref{subsec:Debugging_Technique_Application}.
Finally, \Cref{subsec:Evaluation} evaluates the different approaches.

\subsection{Experiment Setup}
\label{subsec:Experiment_setup}

\begin{figure}
    \vspace{-0.1cm}
    \centering
    \includegraphics[width=6cm]{images/double-lane-change-track.pdf}
    \caption{Double lane change track segment.}
    \vspace{-0.3cm}
  \label{figure:double_lane_chnage}
\end{figure}

In a setup similar to previous research~\cite{DBLP:conf/isorc/RichterGP15}, we use a digital Carrera D132 race track which resembles an environment with typical aspects for the development of mobile embedded devices.
To test the techniques described in \Cref{sec:Debugging_Techniques_Comparison}, we develop a custom slot car controller based on the ESP32 microcontroller.
This allows us to run custom code on a moving slot car and thereby imitate a remote, mobile, and difficult-to-reach embedded device.

Initially, we connect multiple development boards on a board for hardware prototyping (i.e., \emph{perfboard}).
This setup is easy to program and communicate with, using the built-in USB port of the ESP development board.
It is a typical prototyping scenario for embedded devices.
The later revision is implemented as a custom \gls{pcb} with all components integrated and UART access for programming and debugging.
The hardware shows that even relatively simple problems require relatively complex hardware solutions.
Due to the complexity, purely theoretical considerations, such as simulations, cannot easily cover the functionality in interaction with environmental influences.
We assume that this board resembles a product close to market, where development and debugging access is more difficult.
Especially because this \gls{pcb} fits into the chassis of a Carrera slot car, reaching the UART port requires disassembling the car even in the lab.

The slot car is continuously supplied with power over two metal rails (i.e., also when not moving) except on lane change segments, where the rails are interrupted as shown in \Cref{figure:double_lane_chnage}.
On these segments, the slot cars must drive (i.e., roll) up to \textapprox6~cm without a power supply.
These power interruptions must be considered when programming the microcontroller, as program execution has a relevant impact on power consumption.

The overall aim of the development is a self-driving slot car.
It is important to note that the slot car is not supposed to represent an autonomous vehicle.
The debugging data in our experiment form logging data and firmware, which need to be exchanged between the development host and the slot car during the development.
During the power interruptions on lane change segments, energy is limited to the capacity of a 1000~\textmu F capacitor.
The car can sustain normal operation of the microcontroller (clock speed 80~MHz, no WiFi) for up to 100~ms.
Motor power is cut off during these segments, as the motor control is separate from the ESP's power circuitry.

\subsection{Energy Consumption}
\label{subsec:Energy_Consumption}

\begin{figure*}%
    \vspace{-0.3cm}
    \centering
    \subfloat[\centering 80~MHz clock frequency, No WiFi.]{{\includegraphics[width=5cm]{images/80MHz_NoWiFi.tsv.png} }}%
    \qquad
    \subfloat[\centering 80~MHz clock frequency, Not Sending.]{{\includegraphics[width=5cm]{images/80MHz_WiFi_NoSend.tsv.png} }}%
    \qquad
    \subfloat[\centering 80~MHz clock frequency, Sending.]{{\includegraphics[width=5cm]{images/80MHz_WiFi_Send.tsv.png} }}%
    \qquad
    \subfloat[\centering 160~MHz clock frequency, No WiFi.]{{\includegraphics[width=5cm]{images/160MHz_NoWiFi.tsv.png} }}%
    \qquad
    \subfloat[\centering 160~MHz clock frequency, Not Sending.]{{\includegraphics[width=5cm]{images/160MHz_WiFi_NoSend.tsv.png} }}%
    \qquad
    \subfloat[\centering 160~MHz clock frequency, Sending.]{{\includegraphics[width=5cm]{images/160MHz_WiFi_Send.tsv.png} }}%
    \caption{Continuous voltage readout of the track (input) and of the capacitor.}%
    \label{figure:Power_Consumption}%
\end{figure*}

\begin{figure}
    \vspace{-0.3cm}
    \centering
    \includegraphics[width=5cm]{images/240MHz_NoWiFi.tsv.png}
    \caption{240~MHz clock frequency, No WiFi.}
    \vspace{-0.3cm}
  \label{figure:240MHz}
\end{figure}

The energy consumption of the ESP32 is influenced by multiple factors.
During the crossing of a lane change segment, it needs to be tightly controlled, as no power supply is available.
Apart from putting the microcontroller into a sleep mode that prohibits calculation, changing the operating frequency, and limiting the use of wireless communication has a significant impact on the consumed power.

\Cref{figure:Power_Consumption} illustrates the supply and capacitor voltages during the two power interruptions of a lane change segment (at \textapprox 30~ms and \textapprox 120~ms).
The first row, represents the microcontroller operating at 80~MHz clock frequency, the second at 160~MHz.
Individual columns represent (from left to right) operation without the WiFi stack active, with the WiFi stack active but not sending, as well as WiFi active and sending.
The microcontroller is forced to send in column three by repeatedly requesting data from it. % using \textit{curl}.
Requests are timed in a way that they have to be answered exactly while the slot car is on a lane switch segment.

As the supply voltage also encodes the Carrera digital communication protocol, as described in \Cref{subsubsec:Write_to_Carrera_Track}, it fluctuates even during normal operation.
The maximum observed capacitor voltage drop in all scenarios is shown in \Cref{table:Voltage_Drop}.
Where the microcontroller consistently failed operation due to brownouts (capacitor voltage drop $\gtrapprox$~4~V), \textit{n/a} is listed.

Should the capacitor voltage drop below 5~V (the operating voltage of the ESP32) at any point, the brownout detector of the microcontroller detects that, resulting in an automatic reboot of the ESP32 and its peripherals.
As a consequence, data stored non-permanently would be lost and the self-driving would be impaired.
It is apparent that these voltage drops need to be tightly controlled to avoid reboots of the microcontroller.

As also shown in previous work~\cite{DBLP:journals/tvlsi/ZhaiPNHORMHASB09} and hence expected, the operating frequency influences the rate at which the voltage of the capacitor drops as seen in \Cref{figure:Power_Consumption} a) and d) and \Cref{figure:240MHz}.
Furthermore, \Cref{figure:Power_Consumption} b) and e) show that enabling WiFi without actually sending or receiving data slightly increases energy consumption already.
% This is not a significant increase in energy consumption, since the microcontroller is in receive mode, but not continuously queried.
Apparently, the activated WiFi driver stack and chip result in an increased power consumption of 95\textapprox100~mA~\cite{ESP32DataSheet}.

Sending WiFi signals however, is the most problematic scenario as demonstrated in \Cref{figure:Power_Consumption} c) and f) as it draws \textapprox240~mA~\cite{ESP32DataSheet}.
The capacitor voltage drops rapidly in both instances for a very short period of time.
This happens when the microcontroller answers a request from the development host exactly during the first power interruption in both figures.
When using the microcontrollers' maximum frequency of 240~MHz, power consumption is increased again, as shown in \Cref{figure:240MHz}.
The use of WiFi at 240~MHz is infeasible as the car could not cross a lane change segment without brownouting.

\begin{table}
    \centering
        \begin{tabular}{c | ccc}
            & No WiFi & Not Sending & Sending\\
            \hline
            80~MHz & 1.62~V & 1.91~V & 2.64~V\\
            160~MHz & 2.11~V & 2.20~V & 2.82~V\\
            240~MHz & 2.49~V & \textit{n/a} & \textit{n/a}
        \end{tabular}
    \caption{Maximum observed capacitor voltage drop.}
    \vspace{-0.3cm}
    \label{table:Voltage_Drop}
\end{table}

\subsection{Debugging Technique Application}
\label{subsec:Debugging_Technique_Application}

This section describes the application of the debugging techniques described in \Cref{sec:Debugging_Techniques_Comparison}.
After an introductory description of each technique, we discuss their characteristics, give a recommendation for fitting debugging scenarios from \Cref{subsec:Debugging_Scenarios}, and finally evaluating them.

\subsubsection{Save and Print Later (Aperiodic Wired Transmission)}
While driving, the slot car continuously measures data and writes it to internal flash memory.
After a certain period of time, the car stops at a predefined point (e.g., at the start/finish line) and is connected to a read-out device (usually a laptop computer) via a cable.
Then, the car transmits its data via the cable.
At this point, there is also the option to reprogram the car.
When finished, the cable is disconnected, the car deletes the stored data and drives off again.

\textbf{Advantages}
\begin{itemize}
    \item Known method --- easy to set up
    \item Low runtime performance impact
    \item Very low risk of interruption/data corruption
\end{itemize} 

\textbf{Shortcomings}
\begin{itemize}
    \item Stopping not possible everywhere, some parts of track not accessible
    \item Needs physical access, which is difficult inside chassis and UART header is difficult to connect to
    \item Stopping interrupts normal operation --- possibly infeasible in production
    \item Data must be saved whilst driving
\end{itemize}

\textbf{Evaluation}
Overall, "Save and Print Later" is a very simple and reliable form of transferring data from the car as well as uploading firmware to the car.
On the other hand, physical access to the board in the car is required.
The deeper a device is embedded, the more difficult it becomes, and size restrictions also limit the use of the transmission technology.
Therefore it is only a useful solution for early phases in development, where a USB port might exist on the board, and no disassembly is required to reach the data lines.

\subsubsection{Stop and Radio (Aperiodic Wireless Transmission)}
"Stop and Radio" also saves data continuously in order to be able to stop and transfer the data at once.
At this point, it is also possible to reprogram the car.
However, the aperiodic transmission happens wirelessly via the ESP32's built-in 2.4~GHz wireless capabilities (WiFi 802.11 b/g/n, Bluetooth v4.2, and BLE).
The whole stop/transmission/drive-off process now requires no physical access to the device.

\textbf{Advantages}
\begin{itemize}
    \item Stopping possible anywhere on track (except lane changes)
    \item Virtually unlimited energy available, making the transmission time irrelevant
    \item Updates and development are possible
    \item Low risk of interruption
    \item Low runtime performance impact
\end{itemize}

\textbf{Shortcomings}
\begin{itemize}
    \item Device needs to know when to stop --- physical access might be necessary to, e.g., push a button
    \item Stopping interrupts normal operation --- possibly infeasible in production
    \item WiFi has high flash and RAM memory consumption
    \item Data must be saved whilst driving
\end{itemize}

\textbf{Evaluation}
Altogether, "Stop and Radio" trades off memory and compute power for not having to access the device physically compared to "Save and Print Later".
This makes it especially useful for Over-the-Air (OTA) updates and development, especially at later stages of development.

\subsubsection{Write to Carrera Track (Continuous Wired Transmission)}
\label{subsubsec:Write_to_Carrera_Track}

The Carrera digital slot cars are constantly communicating with the control unit of the track via the tracks' power rails using a digital communication protocol.
This way, multiple cars can be in contact with the same rails and can still be individually controlled.
Slot cars can also send some data back, a feature that is usually used by the car to indicate that it reached a sensor.
There is, however, a possibility that these data packets could be hijacked to send custom data, as much as the protocol allows, back onto the track.
This data could then be read and interpreted by a custom decoder, implementing a continuous, wired transmission.

The advantage would be a constant, reliable data output that is less subject to interference like a radio signal might be.
Using this technique, data to be transferred would not have to be saved for extended periods of time but can be sent with minimal delay.
According to the research by Heß~\cite{SlotBaer}, cars can send up to 13~bits, 8 times per 75~ms cycle.
$$\frac{1}{0.075\textrm{~s}} \cdot 8 \cdot 13\textrm{~Bit} \approx 1,387~\frac{\textrm{Bit}}{\textrm{s}}$$
This results in a theoretical bandwidth of $\approx$ 1,387 Bit/s.

\textbf{Advantages}
\begin{itemize}
    \item Continuous output
    \item Position on track does not matter (except lane changes)
\end{itemize}

\textbf{Shortcomings}
\begin{itemize}
    \item Necessary to adhere to Carrera Digital protocol~\cite{SlotBaer}
    \item Low bandwidth
    \item Difficult to set up --- hardware changes needed on both ends
\end{itemize}
\textbf{Evaluation}
In the end, writing to the Carrera track is difficult to set up, both for developers and potential users and offers only extremely limited bandwidth.
Furthermore, for any mobile embedded devices, similar options are not even available.
For this reason, we did not implement this option.

\subsubsection{WiFi On-the-fly (Continuous Wireless Transmission)}
As wireless transmission is possible from anywhere on a slot car track, it allows for a continuous, direct connection to the slot car.
This way, debug output can be sent at all times and from anywhere on the track.
This is similar to writing to the Carrera track but using a wireless medium.
The challenge with this technique is its high power consumption and performance impact, as analyzed by Pötsch et al.~\cite{DBLP:conf/i2mtc/PotschBS17}.
Hence, energy consumption (here, crossing lane change segments) needs special consideration.

\textbf{Advantages}
\begin{itemize}
    \item Continuous output
    \item Position on track does not matter
    \item OTA updates and development possible
\end{itemize}

\textbf{Shortcomings}
\begin{itemize}
    \item High power consumption
    \begin{itemize}
        \item Problematic on lane changes
        \item Unpredictable spikes in power consumption on send
    \end{itemize}
    \item High memory consumption of WiFi stack
    \item WiFi-stack always active --- can have a runtime impact
    \item OTA updates might be interrupted
    \begin{itemize}
        \item Ejection from track, power loss on lane changes
        \item Measures against data corruption necessary
    \end{itemize}
\end{itemize}

\textbf{Evaluation}
This is the most versatile and convenient option, useful for almost any kind of debugging.
However, high energy consumption, memory usage, and more advanced setup make it the most resource-intensive way of communication.
Special precautions must be taken to prevent the microcontroller from consuming too much power when only limited energy is available.
In the worst case, these power interruptions could result in corrupted data or even a corrupted flash image.

\subsection{Evaluation}
\label{subsec:Evaluation}

Except \textit{Write  to  Carrera  Track}, all the presented debugging techniques have been used in different stages of the software development for the self-driving slot car.
The analysis presented in \Cref{subsec:Debugging_Technique_Application} has been conducted during the aforementioned development process and supports the debugging trade-offs identified in \Cref{sec:Debugging_Techniques_Comparison}.

Though being supported by many microcontrollers, the wired transmission of debug logs can have practical issues:
During the intended mode of operation, the device might need to move.
Therefore, attaching cables is either impossible or distorts the operational characteristics, precluding optimization or production scenarios.
Our particular experiment setup would have allowed the loophole of continuous data transmission via a protocol modulated on the power supply.
However, in addition to a relatively high implementation effort, the protocol constraints limit the communication bandwidth to an extent impractical for development or optimization.

In contrast, wireless transmission of debug messages has proven to offer a broader field of feasible application scenarios.
The available bandwidth enabled the transmission of larger volumes of sensor data and verbose status messages during early application development and optimization activities.
A distorting factor on the devices' normal operating conditions has been the increased power consumption of the radio module.
However, our experiment has shown that careful energy budgeting and adaptive control of power states can overcome this obstacle and avoid interference between the regular device operation and the debugging facilities.

%In summary, our experiment shows that achieving a reliable wired connection on a mobile embedded device, like a slot car, is difficult to attain.
%The difficulty is increased, the more embedded the microcontroller becomes.
%With a development board that offers a USB connector, plugging in after a few rounds on the track is feasible.
%Once the device offers only a UART connection that must be plugged in manually, cable by cable, this is much more difficult and risks damaging the hardware.
%Even having the option of sending data wired whilst in the remote working environment is rare and often difficult, like writing data to the Carrera slot car track.
%Therefore, these kinds of devices benefit greatly from the ability to wirelessly transfer data.
%For critical data transfers, like programming and updating the microntroller, we found it is beneficial to attain a reliable power source, which meant stopping the car.
%However, for monitoring, status updates and runtime tweaks, continuous transmission over wireless technology works well, but needs special attention to energy consumption to avoid the risk of running out of power.