\section{Background and Related Work}
\label{sec:Background_and_Related_work}

In this section, we first give an overview of the challenges when debugging mobile embedded devices in \Cref{subsec:Debugging_Mobile_Embedded_Devices}.
\Cref{subsec:Debugging_Scenarios} describes the various scenarios that are derived from the different facets of debugging.
Constraints imposed by mobile embedded devices are outlined in \Cref{subsec:Constraints}.

\subsection{Debugging Mobile Embedded Devices}
\label{subsec:Debugging_Mobile_Embedded_Devices}

Debugging embedded systems is known to be a challenging task~\cite{macnamee2000emerging}, often also adding up constraints from, e.g., (hard) real-time and distributed systems~\cite{DBLP:journals/csur/Stankovic96}.
In the past, researchers have analyzed the debugging of embedded systems in various contexts, e.g., for wireless embedded networks~\cite{DBLP:conf/ipsn/WhitehouseTTSKJHDC06}, multimedia applications~\cite{DBLP:conf/emsoft/CuevaBTMS12}, or on ships~\cite{MacNamee2000EmergingOD}.
As these domains highly differ, the understanding of what debugging includes is different~\cite{IBM:journals/Hailpern02} as well.
This suggests grouping debugging scenarios into different categories with specific requirements.

Data transmission is the key to debugging, as only through communication device states can be determined and diagnosed~\cite{murali2021improved}.
The communication technologies commonly used for embedded devices tend to be different and more diverse compared to those of larger computer systems~\cite{DBLP:conf/codes/SamieBH16}.
Aspects such as resource consumption and timing behavior tend to be of a stronger concern than, e.g., throughput.
At the same time, the wide variety of communication technologies used in embedded systems can be considered characteristic.
In the past, both wired and wireless transmission technologies have been used for transmitting data from embedded devices~\cite{DBLP:conf/codes/SamieBH16, yue2013marine}.
Regarding wireless transmission, there is a large body of research on Bluetooth and WiFi~\cite{yue2013marine, DBLP:conf/infocom/FriedmanKK11}.
Especially for WiFi, extensive experiments on energy consumption have already been carried out~\cite{DBLP:conf/i2mtc/PotschBS17}.

Mobile embedded devices are increasingly deployed in mission-critical applications~\cite{DBLP:conf/hpec/SkowyraBB13}.
The mobility of these devices yields two major challenges:
First, mobile embedded devices might not have a stable energy supply.
Therefore, the energy usage might impact the stability of the system.
At least transiently, energy capacities might be reduced and might even be too little to keep a system functional~\cite{DBLP:conf/aspdac/HwangKJC08}.
Second, not every transmission technology is suitable for every environment. 
While there is a lot of research focusing on the security of mobile embedded devices~\cite{ DBLP:conf/hpec/SkowyraBB13, 5751382, 10.1145/1435458.1435462}, choosing an adequate transmission technology is also dependent on the environment, specifically on energy requirements.

\subsection{Debugging Scenarios}
\label{subsec:Debugging_Scenarios}

Previous research on debugging embedded systems has focused in particular on the development cycle performed by hardware and software developers~\cite{DBLP:conf/uist/DrewNMMMH16, DBLP:conf/chi/BoothSBJ16, DBLP:conf/uist/McGrathWKHDH18, DBLP:conf/osdi/FonsecaDLS08}.
However, as Hailpern et al. point out~\cite{IBM:journals/Hailpern02}, debugging is not limited to software development, but also extends into other parts of the development cycle, including production.
All these different activities require their own capabilities to allow for effective debugging.
In this section, we highlight core development and operation activities during which debugging is required and characterize their requirements respectively.

\subsubsection*{Software Development}
During development, debugging is essential to ensure that the written code does not contain any faults~\cite{IBM:journals/Hailpern02}.
Developers need to conduct experiments to assess how a target platform reacts to certain commands and operating states.
During development, code changes frequently, required debugging output changes frequently, and every hour of development is expensive.
Hence, debugging facilities must be available straightforward, easily adaptable, and must allow for quick code iteration~\cite{DBLP:journals/isj/BaskervilleP04}.

\subsubsection*{Optimization}
Optimization activities increase in the later stages of development.
This is an especially important phase in embedded systems development, where hardware and software tend to be closely coupled.
The hardware is typically close to production-ready, so optimization efforts can take hardware-specifics into account.
The Software is typically already functional but needs to fulfill tighter resource constraints.
As a result, the analysis focuses on identifying capacity and performance bottlenecks as code changes become less frequent~\cite{DBLP:journals/ubiquity/Hyde06}.
Therefore, the debugging instrumentation should have a small and predictable runtime impact.
Ideally, the debugging and optimization techniques are useful for (hard) real-time systems as those systems require minimal impact on program behavior.

\subsubsection*{Monitoring}
During and after development, gaining insight into the embedded systems' states is important to identify misbehaving products and unexpected environmental influences.
For this reason, constantly monitoring certain aspects of embedded devices might be required~\cite{DBLP:journals/spe/DoddR92}.
This can include environmental sensors, hardware states, or dynamic software analyses.
During development, this data should be available quickly, ideally in a continuous stream.
In production, data could be limited to aggregated monitoring reports which are transmitted periodically or logged locally for access in the event of unexpected behavior of the device.
Continuous output is usually not necessary in these scenarios.

\subsubsection*{Operational Status}
The operational status can be derived from monitoring data and summarizes the condition of a device (e.g., commands executed, data processed, resource usage)~\cite{DBLP:journals/cacm/Spinellis18}.
Whilst throughput is of lesser concern, such statuses need to be reported with low latency, as they can indicate problems with the device or critical operating conditions.

\subsubsection*{Defects in Production}
Despite the best efforts of developers, defects in the code might reach the production phase.
As it is unavoidable for humans to err, responsible developers must take measures to be able to detect and debug defects in production code~\cite{IBM:journals/Hailpern02}.
At this point, the distance (physical, as well as virtual) to the device can be challenging.
In case the developer is unable to access a device physically, the transmission of data required for debugging must be automated or made easy for a user to do so.
Additionally, means to deploy new software versions need to be in place.

\subsection{Debugging Constraints}
\label{subsec:Constraints}

Embedded devices are characterized by low computational power as well as little memory availability.
In the case of mobile embedded devices, energy consumption must also be considered. This is of concern especially for battery-operated devices, where increased computational efforts entail increased energy consumption.
Moreover, mobile embedded devices might not always be physically accessible, which can limit the duration, time, and locality for debug data transfer.
Furthermore, if such a device is used in a (hard) real-time system, predictability is also required.
