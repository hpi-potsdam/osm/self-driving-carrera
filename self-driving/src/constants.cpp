#include "constants.h"

#include "RunLengthEncoding.h"
#include "strategy/NullStrategy.h"

RunLengthEncoding track;
RunLengthEncoding measured;

Vector<std::pair<int16_t, int>> timings;

Vector<float> vin_energy;
Vector<float> capacitor_energy;
Vector<float> diffs;

DebugServer server;
Adafruit_MPU6050 mpu;

std::atomic<Strategy*> strategy{&NullStrategy::instance};
