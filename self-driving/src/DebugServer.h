#pragma once

#include <mutex>
#include <WebServer.h>
#include "Config.h"

class DebugServer
{

public:
  DebugServer();

  void emergencyOTA();

  void setup();

  void handleClient();

  void setState(const String &newState);

private:
  void handle_OnConnect();
  void handle_Start();
  void handle_Stop();
  void handle_Diffs();
  void handle_Track();
  void handle_Measured();
  void handle_Energy();
  void handle_Timings();
  
  void handle_Post();

  String htmlContent(const Config &config);

  void redirectTo(const char *location);

  const char *ssid;
  const char *password;

  std::mutex m_state_mutex;
  String m_state;

  TaskHandle_t m_maintenanceTask;

  WebServer m_server;
};
