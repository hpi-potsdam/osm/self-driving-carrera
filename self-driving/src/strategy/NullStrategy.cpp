#include "NullStrategy.h"

#include "../constants.h"
#include "../driving.h"

NullStrategy::NullStrategy() : Strategy("Null Strategy", 0, 0) {}

void NullStrategy::loop()
{
  // server.setState("NULL");
  driving::setSpeed(0);

  // start blinking once we stopped
  if(driving::getSpeed() == 0) {
    ledcWrite(SLED_PWM_CHANNEL, 255);
    delay(200);
    ledcWrite(SLED_PWM_CHANNEL, 0);
    delay(200);
  }
}

NullStrategy NullStrategy::instance;
