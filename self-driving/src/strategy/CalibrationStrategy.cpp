#include "CalibrationStrategy.h"

#include "../constants.h"
#include "../driving.h"
#include "CurveStrategy.h"
#include "NullStrategy.h"
#include "ManualControlStrategy.h"

CalibrationStrategy::CalibrationStrategy() : Strategy("Calibration Strategy", 1, config->slowLoopTime) {}

void CalibrationStrategy::initialize()
{
  server.setState("Calibrate");

  ledcWrite(SLED_PWM_CHANNEL, 0);

  driving::setSpeed(config->calibrationSpeed);

  m_numValues = 0;
  m_measuring = true;

  track.clear();
  track.setSliding(false);
  measured.clear();
  measured.setSliding(false);
  diffs.clear();
  driving::positionGuess = 1;
}

void CalibrationStrategy::loop()
{
  // wait to get up to speed
  if(driving::getSpeed() != config->calibrationSpeed) {
    return;
  }
  else {
    ledcWrite(SLED_PWM_CHANNEL, 255);
  }

  float gyroZ = driving::updateGyroZ(track);
  // populate track history while calibrating as well.
  measured.add(gyroZ);

  m_numValues++;

  if (m_measuring)
  {
    float sum = 0;
    for (size_t i = 0; i < track.size(); ++i)
    {
      sum += track.at(i).length * fabs(track.at(i).strength);
    }
    if (sum > 200)
    {
      m_measuring = false;
    }

    // just to keep diffs consistent with track&measured length
    diffs.push_back(0);
    return;
  }

  float diff = track.diff(track, m_numValues / 2) / m_numValues;
  const int number_of_last_diffs = 4;
  if (diff < config->diffThreshold)
  {
    float last_diffs_sum = 0;
    for (auto i = diffs.size() - 1; i >= diffs.size() - number_of_last_diffs; --i)
    {
      last_diffs_sum += diffs[i];
    }

    float last_diffs_average = last_diffs_sum / number_of_last_diffs;

    if (diff > last_diffs_average)
    {
      strategy.store(&CurveStrategy::instance);
    }
  }

  diffs.push_back(diff);
}

void CalibrationStrategy::teardown()
{
  ledcWrite(SLED_PWM_CHANNEL, 0);

  // track is stored twice, so remove it
  track.pop_first(m_numValues / 2);
  measured.pop_first(m_numValues / 2);
  measured.setSliding(true);
}

CalibrationStrategy CalibrationStrategy::instance;
