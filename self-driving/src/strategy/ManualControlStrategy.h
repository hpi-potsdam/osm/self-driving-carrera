#pragma once

#include "Strategy.h"

class ManualControlStrategy : public Strategy {
public:
  ManualControlStrategy();

  virtual ~ManualControlStrategy() = default;

  void loop() override;

  void initialize() override;

  static ManualControlStrategy instance;
};