#pragma once

#include "Strategy.h"

class StraightStrategy : public Strategy {
public:
  StraightStrategy();
  
  virtual ~StraightStrategy() = default;

  void initialize() override;

  void loop() override;

  void teardown() override;

  static StraightStrategy instance;
};
