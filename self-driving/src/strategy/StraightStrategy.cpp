#include "StraightStrategy.h"

#include "../constants.h"
#include "../driving.h"
#include "StraightEndStrategy.h"

StraightStrategy::StraightStrategy() : Strategy("Straight Strategy", 2, 16666) {}

void StraightStrategy::initialize() {
  server.setState("Straight");

  ledcWrite(SLED_PWM_CHANNEL, 255);
}

void StraightStrategy::loop() {
  float gyroZ = driving::updateGyroZ(measured);

  driving::setSpeed(config->fullSpeed);
  driving::positionGuess += config->drivenLength;

  if(!(track.preview(driving::positionGuess) < config->curveThreshold && (gyroZ > -config->curveThreshold && gyroZ < config->curveThreshold))) {
    strategy.store(&StraightEndStrategy::instance);
  }
}

void StraightStrategy::teardown() {
  ledcWrite(SLED_PWM_CHANNEL, 0);
}

StraightStrategy StraightStrategy::instance;
