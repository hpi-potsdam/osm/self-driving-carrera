#pragma once

#include <WString.h>

class Strategy
{
public:
  virtual ~Strategy() = default;

  virtual void initialize() {}

  virtual void loop() = 0;

  virtual void teardown() {}

  const String &getStrategyName() const
  {
    return m_strategyName;
  }

  const int16_t getStrategyId() const
  {
    return m_strategyId;
  }

  int getLoopTime() const
  {
    return m_loopTime;
  }

protected:
  Strategy(const String &strategyName, const int16_t strategyId, int loopTime) : m_strategyName{strategyName}, m_strategyId{strategyId}, m_loopTime(loopTime) {}

  String m_strategyName;
  int16_t m_strategyId;

  int m_loopTime;
};