#include "CurveStrategy.h"

#include "../constants.h"
#include "../driving.h"
#include "../RunLengthEncoding.h"
#include "StraightStrategy.h"
#include <algorithm>
#include <limits>

CurveStrategy::CurveStrategy() : Strategy("Curve Strategy", 4, config->slowLoopTime) {}

void CurveStrategy::initialize()
{
  server.setState("In curve");
  driving::setSpeed(config->minSpeed);
}

void CurveStrategy::loop()
{
  driving::updateGyroZ(measured);

  float minDiff = std::numeric_limits<float>::infinity();
  int minIndex;

  for (int i = -5; i <= 5; ++i)
  {
    float diff = track.diff(measured, driving::positionGuess + i);
    if (diff < minDiff)
    {
      minDiff = diff;
      minIndex = i;
    }
  }

  driving::positionGuess += minIndex;

  // Reduce the positionGuess size as a performance optimization
  // Otherwise performance degrades contiuously, as the modulo operators on
  // an ESP32 is pretty expensive

  while (driving::positionGuess > track.valuesSize())
  {
    driving::positionGuess -= track.valuesSize();
  }
  while (driving::positionGuess < 0)
  {
    driving::positionGuess += track.valuesSize();
  }

  //if(!(track.preview(driving::positionGuess) / config->numPreviews >= config->curveThreshold)) {
  //  strategy.store(&StraightStrategy::instance);
  //}
  auto preview = track.preview(driving::positionGuess - 2);

  // preview is somewhere between 0 to 5, based on our measurements.
  // 0 = straight
  // 5 = hard curve
  ledcWrite(SLED_PWM_CHANNEL, std::min(255, static_cast<int>(4 * preview)));

  // we use minSpeed, as our experience shows that that speed is
  // safe for any curve.

  auto newSpeed = (config->maxCurve - preview) * (config->fullSpeed - config->minSpeed) / config->maxCurve + config->minSpeed;
  newSpeed = std::min(std::max(newSpeed, 0.0f), 255.0f); // clamp between 0 and 255 just to be safe.
  driving::setSpeed(newSpeed);

  // Modulate time, so that we pass the same distance during each time step, no matter our speed.
  const auto new_timing = (config->fullSpeed - newSpeed) * ((config->slowLoopTime - config->fastLoopTime) / (config->fullSpeed - config->minSpeed)) + config->fastLoopTime;
  m_loopTime = new_timing;
}

CurveStrategy CurveStrategy::instance;
