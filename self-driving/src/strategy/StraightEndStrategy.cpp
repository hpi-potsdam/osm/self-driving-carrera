#include "StraightEndStrategy.h"

#include "../constants.h"
#include "../driving.h"
#include "CurveStrategy.h"

StraightEndStrategy::StraightEndStrategy() : Strategy("Straight End Strategy", 3, 16666) {}

void StraightEndStrategy::initialize() {
  server.setState("Entering curve");
}

void StraightEndStrategy::loop() {
  driving::setSpeed(config->minSpeed);
  float gyroZ = driving::updateGyroZ(measured);
  driving::positionGuess += config->drivenLength;

  if(!(gyroZ > -config->curveThreshold && gyroZ < config->curveThreshold)) {
    strategy.store(&CurveStrategy::instance);
  }
}

StraightEndStrategy StraightEndStrategy::instance;
