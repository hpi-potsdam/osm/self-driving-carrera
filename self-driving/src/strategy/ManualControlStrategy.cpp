#include "ManualControlStrategy.h"

#include "../constants.h"
#include "../DebugServer.h"
#include "../driving.h"

#include <esp_types.h>
#include <stdlib.h>
#include <ctype.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/timers.h"
#include "esp_log.h"
#include "esp_pm.h"
#include "soc/rtc.h"
#include "soc/sens_reg.h"
#include "soc/sens_struct.h"
#include "driver/rtc_io.h"
#include "sys/lock.h"
#include "driver/gpio.h"
#include "driver/adc.h"

#include <driver/adc.h>

ManualControlStrategy ManualControlStrategy::instance;

ManualControlStrategy::ManualControlStrategy() : Strategy("Manual Control Strategy", 6, 0) {}

void ManualControlStrategy::initialize()
{
    server.setState("ManualControl");

    driving::setSpeed(0);

    vin_energy.clear();
    capacitor_energy.clear();

    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_4, ADC_ATTEN_DB_11);
}

// this function should be called in the critical section
static int adc1_convert(int channel)
{
    uint16_t adc_value;
    SENS.sar_meas_start1.sar1_en_pad = (1 << channel); //only one channel is selected.
    while (SENS.sar_slave_addr1.meas_status != 0);
    SENS.sar_meas_start1.meas1_start_sar = 0;
    SENS.sar_meas_start1.meas1_start_sar = 1;
    while (SENS.sar_meas_start1.meas1_done_sar == 0);
    adc_value = SENS.sar_meas_start1.meas1_data_sar;
    return adc_value;
}

int adc1_get_raw_no_lock(adc1_channel_t channel)
{
    //start conversion
    return adc1_convert(channel);
}

void ManualControlStrategy::loop() {
    // initializes ADC1
    adc1_get_raw(ADC1_CHANNEL_4);

    while(vin_energy.size() < vin_energy.max_size()) {
        vin_energy.push_back(adc1_get_raw_no_lock(ADC1_CHANNEL_4));
    }
    while(capacitor_energy.size() < capacitor_energy.max_size()) {
        capacitor_energy.push_back(0);
    }

    server.setState(String(vin_energy.size()));
}