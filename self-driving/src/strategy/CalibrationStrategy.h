#pragma once

#include "Strategy.h"

class CalibrationStrategy : public Strategy
{
public:
  CalibrationStrategy();

  virtual ~CalibrationStrategy() = default;

  void loop() override;

  void initialize() override;

  void teardown() override;

  static CalibrationStrategy instance;

protected:
  int m_numValues = 0;
  bool m_measuring;
};
