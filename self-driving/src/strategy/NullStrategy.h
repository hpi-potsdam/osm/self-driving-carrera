#pragma once

#include "Strategy.h"

class NullStrategy : public Strategy {
public:
  NullStrategy();

  virtual ~NullStrategy() = default;

  void loop() override;

  static NullStrategy instance;
};
