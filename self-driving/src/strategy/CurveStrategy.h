#pragma once

#include "Strategy.h"

class CurveStrategy : public Strategy
{
public:
  CurveStrategy();
  
  virtual ~CurveStrategy() = default;

  void initialize() override;

  void loop() override;

  static CurveStrategy instance;
};
