#pragma once

#include "Strategy.h"

class StraightEndStrategy : public Strategy {
public:
  StraightEndStrategy();

  virtual ~StraightEndStrategy() = default;

  void initialize() override;
  
  void loop() override;

  static StraightEndStrategy instance;
};
