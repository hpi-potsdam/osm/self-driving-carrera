#include "DebugServer.h"

#include <WiFi.h>
#include <ArduinoOTA.h>
#include <functional>

#include "Config.h"
#include "constants.h"
#include "RunLengthEncoding.h"
#include "strategy/CalibrationStrategy.h"
#include "strategy/NullStrategy.h"

DebugServer::DebugServer() : m_server{80},
                             ssid{"🛸👽☢💥☠️"},
                             password{"CurrentlySettingUpWifi"}
{
  setState("");
}

void DebugServer::emergencyOTA()
{
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");

  delay(100);

  ArduinoOTA
      .onStart([]()
               {
                 String type;
                 if (ArduinoOTA.getCommand() == U_FLASH)
                   type = "sketch";
                 else // U_SPIFFS
                   type = "filesystem";

                 // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                 Serial.println("Start updating " + type);
               })
      .onEnd([]()
             { Serial.println("\nEnd"); })
      .onProgress([](unsigned int progress, unsigned int total)
                  { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); })
      .onError([](ota_error_t error)
               {
                 Serial.printf("Error[%u]: ", error);
                 if (error == OTA_AUTH_ERROR)
                   Serial.println("Auth Failed");
                 else if (error == OTA_BEGIN_ERROR)
                   Serial.println("Begin Failed");
                 else if (error == OTA_CONNECT_ERROR)
                   Serial.println("Connect Failed");
                 else if (error == OTA_RECEIVE_ERROR)
                   Serial.println("Receive Failed");
                 else if (error == OTA_END_ERROR)
                   Serial.println("End Failed");
               });

  ArduinoOTA.begin();

  while (true)  
  {
    ArduinoOTA.handle();
    delay(10);
  }
}

void DebugServer::setState(const String &newState)
{
  std::lock_guard<std::mutex> state_lock(m_state_mutex);
  m_state = newState;
}

void DebugServer::setup()
{
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");

  delay(100);

  m_server.on("/", std::bind(&DebugServer::handle_OnConnect, this));
  m_server.on("/stop", std::bind(&DebugServer::handle_Stop, this));
  m_server.on("/start", std::bind(&DebugServer::handle_Start, this));
  m_server.on("/diffs", std::bind(&DebugServer::handle_Diffs, this));
  m_server.on("/track", std::bind(&DebugServer::handle_Track, this));
  m_server.on("/measured", std::bind(&DebugServer::handle_Measured, this));
  m_server.on("/postconfig", std::bind(&DebugServer::handle_Post, this));
  m_server.on("/energy", std::bind(&DebugServer::handle_Energy, this));
  m_server.on("/timings", std::bind(&DebugServer::handle_Timings, this));
  m_server.begin();
  Serial.println("HTTP server started");

  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
      .onStart([]()
               {
                 String type;
                 if (ArduinoOTA.getCommand() == U_FLASH)
                   type = "sketch";
                 else // U_SPIFFS
                   type = "filesystem";

                 // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                 Serial.println("Start updating " + type);
               })
      .onEnd([]()
             { Serial.println("\nEnd"); })
      .onProgress([](unsigned int progress, unsigned int total)
                  { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); })
      .onError([](ota_error_t error)
               {
                 Serial.printf("Error[%u]: ", error);
                 if (error == OTA_AUTH_ERROR)
                   Serial.println("Auth Failed");
                 else if (error == OTA_BEGIN_ERROR)
                   Serial.println("Begin Failed");
                 else if (error == OTA_CONNECT_ERROR)
                   Serial.println("Connect Failed");
                 else if (error == OTA_RECEIVE_ERROR)
                   Serial.println("Receive Failed");
                 else if (error == OTA_END_ERROR)
                   Serial.println("End Failed");
               });

  ArduinoOTA.begin();

  xTaskCreatePinnedToCore(
      (void (*)(void *)) & DebugServer::handleClient, /* Task function. */
      "Server::HandleClient",                         /* name of task. */
      10000,                                          /* Stack size of task */
      this,                                           /* parameter of the task */
      1,                                              /* priority of the task */
      &m_maintenanceTask,                             /* Task handle to keep track of created task */
      0);                                             /* pin task to core 0 */
}

const char *htmlContentStart = R"~~~(
<!doctype html>
<html lang=en>
    <head>
        <meta charset=utf-8>
        <title>Carrera Car Control Center (CCCC)</title>
    </head>
    <body>
        <a href='./start'><p>Start</p></a>
        <a href='./stop'><p>Stop</p></a>
        <a href='./diffs'><p>Diffs</p></a>
        <a href='./track'><p>Track</p></a>
        <a href='./measured'><p>Measured</p></a>
        <form method="post" nctype="application/x-www-form-urlencoded" action="/postconfig">
)~~~";

const char *htmlContentEnd = R"~~~(
          <input type="submit" value="Submit"/>
        </form><br/>
    </body>
</html>
)~~~";

String DebugServer::htmlContent(const Config &config)
{
  std::lock_guard<std::mutex> state_lock(m_state_mutex);
  return String(htmlContentStart) +
         m_state + "<br/>" +
         "Calibration Speed: <input type='number' name='calibrationSpeed' value='" + config.calibrationSpeed + "'/><br/>" +
         "Full Speed: <input type='number' name='fullSpeed' value='" + config.fullSpeed + "'/><br/>" +
         "Minimum Speed: <input type='number' name='minSpeed' value='" + config.minSpeed + "'/><br/>" +
         "Segment Threshold: <input type='number' name='segmentThreshold' step='0.1' value='" + config.segmentThreshold + "'/><br/>" +
         "Curve Threshold: <input type='number' name='curveThreshold' step='0.1' value='" + config.curveThreshold + "'/><br/>" +
         "Slow Looptime: <input type='number' name='slowLoopTime' step='1' value='" + config.slowLoopTime + "'/><br/>" +
         "Fast Looptime: <input type='number' name='fastLoopTime' step='1' value='" + config.fastLoopTime + "'/><br/>" +
         "Previews: <input type='number' name='numPreviews' step='1' value='" + config.numPreviews + "'/><br/>" +
         htmlContentEnd;
}

String diffsContent()
{
  String result = "diffs\n";
  for (const auto &diff : diffs)
  {
    result += String(diff) + "\n";
  }
  return result;
}

String rleContent(RunLengthEncoding &rle)
{
  String result = "strength\tlength\n";
  for (size_t i = 0; i < rle.size();  ++i)
  {
    const auto& entry = rle.at(i);
    if (entry.length == 0)
    {
      break;
    }
    result += String(entry.strength) + "\t" + String(entry.length) + "\n";
  }
  return result;
}

void DebugServer::handle_Post()
{
  if (m_server.method() != HTTP_POST)
  {
    m_server.send(405, F("text/plain"), F("Method Not Allowed"));
  }
  else
  {
    String message = F("POST form was:\n");

    if (m_server.hasArg("calibrationSpeed"))
    {
      config->calibrationSpeed = m_server.arg("calibrationSpeed").toInt();
    }
    if (m_server.hasArg("minSpeed"))
    {
      config->minSpeed = m_server.arg("minSpeed").toInt();
    }
    if (m_server.hasArg("fullSpeed"))
    {
      config->fullSpeed = m_server.arg("fullSpeed").toInt();
    }
    if (m_server.hasArg("segmentThreshold"))
    {
      config->segmentThreshold = m_server.arg("segmentThreshold").toFloat();
    }
    if (m_server.hasArg("curveThreshold"))
    {
      config->curveThreshold = m_server.arg("curveThreshold").toFloat();
    }
    if (m_server.hasArg("slowLoopTime"))
    {
      config->slowLoopTime = m_server.arg("slowLoopTime").toInt();
    }
    if (m_server.hasArg("fastLoopTime"))
    {
      config->fastLoopTime = m_server.arg("fastLoopTime").toInt();
    }
    if (m_server.hasArg("numPreviews"))
    {
      config->numPreviews = m_server.arg("numPreviews").toInt();
    }

    redirectTo("/");
  }
}

void DebugServer::handleClient()
{
  while (true)
  {
    m_server.handleClient();
    ArduinoOTA.handle();
    delay(100);
  }
}

void DebugServer::handle_OnConnect()
{
  m_server.send(200, "text/html", htmlContent(*config));
}

void DebugServer::handle_Diffs()
{
  m_server.send(200, "text/csv", diffsContent());
}

void DebugServer::handle_Track()
{
  m_server.send(200, "text/csv", rleContent(track));
}

void DebugServer::handle_Measured()
{
  m_server.send(200, "text/csv", rleContent(measured));
}

void DebugServer::handle_Energy()
{
  String result = "vin\tcapacitor\n";
  for (auto i = 0; i < vin_energy.size() && i < capacitor_energy.size(); i++)
  {
    result += String(vin_energy.at(i)) + "\t" + String(capacitor_energy.at(i)) + "\n";
  }

  m_server.send(200, "text/csv", result);
}

void DebugServer::handle_Timings()
{
  String result = "Strategy\tTiming\n";
  for (const auto& timing : timings)
  {
    result += String(timing.first) + "\t" + String(timing.second) + "\n";
  }

  m_server.send(200, "text/csv", result);
}

void DebugServer::handle_Stop()
{
  strategy.store(&NullStrategy::instance);
  redirectTo("/");
}

void DebugServer::handle_Start()
{
  strategy.store(&CalibrationStrategy::instance);
  redirectTo("/");
}

void DebugServer::redirectTo(const char *location)
{
  m_server.sendHeader("Location", location, true);
  m_server.send(302, "text/plane", "");
}
