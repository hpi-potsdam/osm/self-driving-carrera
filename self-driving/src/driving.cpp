#include "driving.h"

#include "constants.h"
#include "RunLengthEncoding.h"

float targetSpeed = 0;
float realSpeed = 0;

void decelerate(uint32_t duty) {
    ledcWrite(MOTOR_PWM_CHANNEL, 0);
    delay(1);
    ledcWrite(MOTOR_BRK_PWM_CHANNEL, duty);
}

void accelerate(uint32_t duty) {
    ledcWrite(MOTOR_BRK_PWM_CHANNEL, 0);
    delay(1);
    ledcWrite(MOTOR_PWM_CHANNEL, duty);

}

void decelerate(uint32_t duty) {
    ledcWrite(MOTOR_PWM_CHANNEL, 0);
    delay(1);
    ledcWrite(MOTOR_BRK_PWM_CHANNEL, duty);
}

void accelerate(uint32_t duty) {
    ledcWrite(MOTOR_BRK_PWM_CHANNEL, 0);
    delay(1);
    ledcWrite(MOTOR_PWM_CHANNEL, duty);

}

void driving::setSpeed(int newSpeed) {
  // If the speed get's too low, the motor will block and potentially burn out.
  // Therefore, just completely stop the motor in this case.
  if(newSpeed < config->minSpeed) {
    newSpeed = 0;
  }

  // ledcWrite(MOTOR_PWM_CHANNEL, newSpeed);
  targetSpeed = newSpeed;
}

void driving::tick(float milliseconds) {
  if (targetSpeed > realSpeed) {
    accelerate(255);
    realSpeed += config->acceleration * (milliseconds / 1000);
    if (targetSpeed <= realSpeed) {
      realSpeed = targetSpeed;
    }
  }
  else if(targetSpeed < realSpeed) {
    decelerate(255);
    realSpeed -= config->deceleration * (milliseconds / 1000);

    if(realSpeed >= targetSpeed) {
      realSpeed = targetSpeed;
    }
  }
  else {
    // targetSpeed and realSpeed are equal
    accelerate(targetSpeed);
  }
}


int driving::getSpeed() {
  return realSpeed;
}

float driving::positionGuess = 1;

float driving::updateGyroZ(RunLengthEncoding& rle) {
  sensors_event_t a, g, temp;
  
  const int average_count = 5;
  float acc_x = 0;
  float acc_y = 0;
  float gyro_z = 0;

  for (int i = 0; i < average_count; i++) {
    mpu.getEvent(&a, &g, &temp);
    gyro_z += g.gyro.z;
  }
  gyro_z /= average_count;

  rle.add(gyro_z);

  return gyro_z;
}