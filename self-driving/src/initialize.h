#pragma once

namespace initialize {
  void emergencyOTA();
  void io();
  void peripherals();
  void power();
  void debugServer();
  void storages();
  void strategy();
} // initialize
