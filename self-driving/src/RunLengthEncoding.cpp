#include <algorithm>
#include <Arduino.h>

#include "Config.h"
#include "RunLengthEncoding.h"
#include "constants.h"

RunLengthEncoding::RunLengthEncoding() : m_sliding(false),
                                         m_beginningIndex(0),
                                         m_endIndex(0),
                                         m_valuesSize(0)
{
}

bool RunLengthEncoding::isDifferenceSmallerThanThreshold(float lastStrength, float data)
{
    const float threshold = 0.7;
    return abs(lastStrength - data) < threshold;
}

void RunLengthEncoding::add(float data)
{
    if (size() == 0)
    {
        emplace_back(Encoding(data, 1));
        m_valuesSize++;
        return;
    }

    Encoding &lastElement = encodings[m_endIndex - 1];
    if (isDifferenceSmallerThanThreshold(lastElement.strength, data))
    {
        lastElement.strength = (lastElement.strength * lastElement.length + data) / (lastElement.length + 1);
        lastElement.length++;
    }
    else
    {
        emplace_back(Encoding(data, 1));
    }
    m_valuesSize++;

    if (m_sliding)
    {
        auto &firstElement = encodings[m_beginningIndex];
        --firstElement.length;
        if (firstElement.length == 0)
        {
            m_beginningIndex = (m_beginningIndex + 1) % numEncodings;
        }
        m_valuesSize--;
    }
}

void RunLengthEncoding::clear()
{
    m_beginningIndex = 0;
    m_endIndex = 0;
    for(size_t i = 0; i < numEncodings; i++) {
        encodings[i] = Encoding(0, 0);
    }
}

size_t RunLengthEncoding::size() const
{
    return m_endIndex >= m_beginningIndex ? m_endIndex - m_beginningIndex : m_endIndex + numEncodings - m_beginningIndex;
}
size_t RunLengthEncoding::valuesSize() const
{
    return m_valuesSize;
}

void RunLengthEncoding::emplace_back(Encoding &&encoding)
{
    encodings[m_endIndex] = encoding;
    m_endIndex = (m_endIndex + 1) % numEncodings;
}

void RunLengthEncoding::pop_first(size_t numValues)
{
    while (numValues > 0 && size() > 0) {
        auto &length = encodings[m_beginningIndex].length;
        if (length <= numValues)
        {
            m_beginningIndex = (m_beginningIndex + 1) % numEncodings;
            numValues -= length;
            m_valuesSize -= length;
        }
        else {
            length -= numValues;
            m_valuesSize -= numValues;
            numValues = 0;
        }
    }
}

void RunLengthEncoding::setSliding(bool value)
{
    m_sliding = value;
}

const Encoding &RunLengthEncoding::at(int index) const
{
    // allow negative input up to -size(), allow arbitrary overshoot, except if RLE is empty
    while(index < 0) {
        index += size();
    }
    index %= size();

    // shift to internal index range
    index = (index + m_beginningIndex) % numEncodings;

    return encodings[index];
}

void RunLengthEncoding::findIndex(int guess, int &trackIndex, uint16_t &trackOffset)
{
    // The positionGuess may be negative, so move it into our index range.
    while(guess < 0) {
        guess += valuesSize();
    }

    trackIndex = 0;
    while (guess > at(trackIndex).length)
    {
        guess -= at(trackIndex).length;
        trackIndex++;
    }
    trackOffset = guess;
}

float RunLengthEncoding::diff(RunLengthEncoding &measured, int guess)
{
    guess = max(guess, 1);
    int trackIndex = 0;
    uint16_t trackOffset = 0;
    findIndex(guess, trackIndex, trackOffset);

    int measuredIndex = measured.size() - 1;
    uint16_t measuredOffset = measured.at(measuredIndex).length;

    float diff = 0;
    while (measuredIndex >= 0 && guess >= 0)
    {
        int minOffset = min(trackOffset, measuredOffset);
        diff += fabs(at(trackIndex).strength - measured.at(measuredIndex).strength) * minOffset;

        trackOffset -= minOffset;
        measuredOffset -= minOffset;
        guess -= minOffset;

        if (trackOffset == 0)
        {
            trackIndex--;
            trackOffset = at(trackIndex).length;
        }
        if (measuredOffset == 0)
        {
            measuredIndex--;
            measuredOffset = measured.at(measuredIndex).length;
        }
    }

    return diff;
}

// float RunLengthEncoding::preview(float guess)
// {
//     int trackIndex;
//     uint16_t trackOffset;
//     findIndex(guess, trackIndex, trackOffset);

//     int remainingPreviews = config->numPreviews;
//     size_t result = 0;

//     while (remainingPreviews > at(trackIndex).length - trackOffset)
//     {
//         result += fabs(at(trackIndex).strength) * (at(trackIndex).length - trackOffset);
//         remainingPreviews -= at(trackIndex).length - trackOffset;
//         trackIndex++;
//         trackOffset = 0;
//     }
//     result += fabs(at(trackIndex).strength) * remainingPreviews;

//     return result / config->numPreviews;
// }

float RunLengthEncoding::preview(float guess)
{
    guess += config->previewOffset;
    int trackIndex;
    uint16_t trackOffset;
    findIndex(guess, trackIndex, trackOffset);

    int remainingPreviews = config->numPreviews;
    float result = 0;

    while (remainingPreviews > at(trackIndex).length - trackOffset)
    {
        result = std::max(result, fabsf(at(trackIndex).strength));
        remainingPreviews -= at(trackIndex).length - trackOffset;
        trackIndex++;
        trackOffset = 0;
    }
    result = std::max(result, fabsf(at(trackIndex).strength));

    return result;
}