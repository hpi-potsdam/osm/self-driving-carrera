#pragma once

#include <Arduino.h>
#include <Vector.h>

#define HISTORY_LENGTH 10

struct Encoding
{
  float strength; // encodes the "direction" of the data => you can see if it is right, left or straight
  uint16_t length;

  Encoding() = default;
  Encoding(float str, uint16_t len) : strength{str}, length{len} {}
};

class RunLengthEncoding
{
public:
  RunLengthEncoding();

  void add(float data);

  const Encoding &at(int index) const;

  void clear();

  size_t size() const;
  size_t valuesSize() const;

  void emplace_back(Encoding &&encoding);

  void pop_first(size_t numValues);

  void setSliding(bool value);

  float diff(RunLengthEncoding &measured, int guess);

  float preview(float guess);

private:
  // public, because iteration over pairs of length and value is easier this way.
  static constexpr size_t numEncodings = 1024;
  Encoding encodings[numEncodings];

  bool isDifferenceSmallerThanThreshold(float lastStrength, float data);

  void findIndex(int guess, int &trackIndex, uint16_t &trackOffset);

  bool m_sliding;
  size_t m_beginningIndex;
  size_t m_endIndex;
  size_t m_valuesSize;
};
