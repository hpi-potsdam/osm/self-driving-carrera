/*
 *
 * CC - Björn Daase, Leon Matthes
 *
 * SCOPE
 * 
 * This is the main source file fo the self-driving slot car.
 * It was designed for the hardware of Rev.1 (Antonov)
 * but forwards compatible with the Rev.2 (Boeing).
 * For now, it mainly focusses on the essentials for a working
 * self driving slot-car and does not yet implement the control
 * of miscellaneous, external LEDs and stuff.
 * It currently controls:
 *  - Motor Management
 *  - WiFi Antenna for managing the car
 *  - Reading MPU data
 *  - One Status LED
 *  - Input of track data
 *  - Input of energy data  
 *  - Infrared LED
 * 
 * SLOT CAR CONTROL
 * 
 * The Carrera Car Control Center (CCCC) allows you to manage
 * the slot car. It acts as an interface to  control various
 * magic values and parameters for the steering of the car
 * as well as it allows you to get track, energy, driving... 
 * data. Furthermore you can use the WiFi Stack to do OTA
 * updates (at least with the Arduino IDE).
 * Therfore, please adjust the WiFi Credential in
 * `src/DebugServer.cpp` initially to match yours, program
 * the car once via UART and afterwards you can use OTA updates.
 * 
 * STRATEGIES
 * 
 * All phases the car is in are represented by so called strategies,
 * decieding what a car is doing. You can find them in `src/strategy`.
 * Currently, only NullStrategy, CalibrationStrategy and CurveStrategy
 * are used. See `src/strategy/strategy.h` for how a strategy looks
 * like. The major thing of a strategy is it's loop, in which it can
 * control what it does.
 * Strategies can control the `loopTime`, which determines every how
 * often the loop of the current strategy is called. This can be used
 * to simulate a time modulation, which, for example, requires the
 * CurveStrategy to take into account different speeds.
 * 
 * CALLIBRATION PHASE
 * 
 * The self-driving car generally operates in two strategies: The first
 * phase is the CallibrationStrategy in which the car reads the driven
 * track and continuously diffs the first half of the track against
 * the second half. To save space, the track data is saved as a 
 * RunLengthEncoding. When the diff falls under a given threshold, the
 * car made two laps. Then, it changes into the CurveStrategy.
 * 
 * DRIVING PHASE
 * 
 * CurveStrategy is a historic name for the main driving strategy,
 * which now also handles straights. It works as follows: After finishing
 * the callibration phase, the car knows exactly where on the track it is.
 * Now, every loop, the car diffs the last x values it has gotten from the
 * MPU against its current position +/- a little deviation. Wherever
 * the result is the lowest, the car expects itself to be there. This then
 * allows the cat to look into the future from its caluclated position
 * futher in the track array to have a look "at the future", which allows
 * it to modulate its speed. The modulation is done between a maximum speed
 * (for straights) and a minimum speed (for hard curves). When it increases
 * its speed, it also decreases its loopTime (using a heuristic), making sure
 * two measure points in the track array actually represent the same time
 * span (due to the larger distance traveled at a higher speed).
 * 
 * STANDING STILL
 * 
 * The NullStrategy lets the car stand at its current position. This is
 * mainly useful if you want tro transfer data from the car and want to
 * avoid crashing when crossing lane segment changes or when adjusting
 * the magic values for driving.
 * 
 */

#include <Arduino.h>

#include "src/Config.h"
#include "src/constants.h"
#include "src/initialize.h"
#include "src/strategy/Strategy.h"
#include "src/strategy/NullStrategy.h"
#include "src/driving.h"

Strategy *currentStrategy = nullptr;

void setup()
{
  initialize::io();
  initialize::emergencyOTA();
  initialize::peripherals();
  initialize::power();
  initialize::debugServer();
  initialize::storages();
  initialize::strategy();

  currentStrategy = strategy.load();

  vTaskPrioritySet(NULL, configMAX_PRIORITIES - 1);
}

void loop()
{
  const auto start_time = micros();

  currentStrategy->loop();

  const auto loopTime = currentStrategy->getLoopTime();
  
  const auto end_time = micros();
  auto duration = end_time - start_time;

  if (loopTime != 0)
  {
    if (timings.size() < timings.max_size())
    {
      timings.push_back(std::make_pair(currentStrategy->getStrategyId(), duration));
    }

    if (duration < loopTime)
    {
      delayMicroseconds(loopTime - duration);
      duration = loopTime;
    }
    else
    {
      //strategy.store(&NullStrategy::instance);
      server.setState(currentStrategy->getStrategyName() + " overshot deadline by " + (duration - loopTime) + " us.\n");
    }
  }
  driving::tick(duration / 1000);

  auto nextStrategy = strategy.load();
  if (currentStrategy != nextStrategy)
  {
    currentStrategy->teardown();
    nextStrategy->initialize();
    currentStrategy = nextStrategy;
  }
}
