#include <Adafruit_MPU6050.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <math.h>

#ifdef ESP32
#define MOTOR_PIN 12
#define MOTOR_PWM_CHANNEL 0
#else
#define MOTOR_PIN 9
#endif




void setup() {
  Wire.begin(4, 15);
  Serial.begin(115200);
  while (!Serial);
  Serial.println("MPU6050 OLED demo");

  if (!mpu.begin()) {
    Serial.println("Sensor init failed");
    while (1)
      yield();
  }
  Serial.println("Found a MPU-6050 sensor");
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }
  display.display();
  delay(500); // Pause for 2 seconds
  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setRotation(0);

  // set motor to default speed
  int speed = 200;
  #ifdef ESP32
  ledcSetup(MOTOR_PWM_CHANNEL, 5000/*Hz*/, /*resolution (bit)*/ 8);
  ledcAttachPin(MOTOR_PIN, MOTOR_PWM_CHANNEL);
  ledcWrite(MOTOR_PWM_CHANNEL, speed);

  #else 
  pinMode(MOTOR_PIN, OUTPUT);
  analogWrite(MOTOR_PIN, speed);
  #endif

  #ifdef ESP32
  setCpuFrequencyMhz(80);
  Serial.print("CPU Freq: ");
  Serial.println(getCpuFrequencyMhz());
 #endif
}

void loop() {
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

 /* Serial.print(a.acceleration.x, 1);
  Serial.print("\t");
  Serial.print(a.acceleration.y, 1);
  Serial.print("\t");
  Serial.println(a.acceleration.z - 9.8, 1);*/


  /*display.clearDisplay();
  display.setCursor(0, 0); //display.println("Accelerometer - m/s^2");
  
  display.fillRect(0, 0, fabs(x) * 20, 20, 0xFFFF);
  //display.print(", ");
  display.fillRect(0, 30, fabs(y) * 10, 20, 0xFFFF);
  //display.print(", ");
  //display.println(fabs(a.acceleration.z), 1);
  //display.println("");*/

  Serial.print(g.gyro.x, 1);
  Serial.print("\t");
  Serial.print(g.gyro.y, 1);
  Serial.print("\t");
  Serial.println(g.gyro.z, 1);

  /*display.println("Gyroscope - rps");
  display.print(g.gyro.x, 1);
  display.print(", ");
  display.print(g.gyro.y, 1);
  display.print(", ");
  display.print(g.gyro.z, 1);
  display.println("");*/

  //display.display();
}
