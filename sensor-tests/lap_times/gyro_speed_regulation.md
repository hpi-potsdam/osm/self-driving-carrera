

## minSpeed: 160
maxGyro: 4.2
LapTime: 8.4s

maxGyro: 4.7
LapTime: 7.8s

maxGyro: 4.4
LapTime: 7.9s

maxGyro: 4.5
LapTime: 8
notes: schwenkt unterschiedlich stark aus, stellenweise zu stark


## minSpeed: 200
maxGyro: 3.5
LapTime: 7

maxGyro: 3.7
LapTime: 6.87

maxGyro: 3.6
LapTime: 6.96
LapTime: 6.86
Note: Kalter Motor, schert stark aus


## minSpeed: 160
maxGyro: 3.5
LapTime: 7.2

maxGyro: 3.6
LapTime: 7.1

maxGyro: 4.0
LapTime: 6.82
LapTime: 6.88
Note: Schert leicht aus
