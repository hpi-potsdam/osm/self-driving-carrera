//#define DEBUG_DISPLAY
#include <Arduino.h>
#include "RunLengthEncoding.h"


#include "I2Cdev.h"

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#ifdef DEBUG_DISPLAY
#include "SSD1306Wire.h"
#endif

#include "MPU6050_6Axis_MotionApps20.h"


#ifdef ESP32
#include "esp_bt_main.h"
#include "esp_bt.h"
#include "esp_wifi.h"
#include "esp_pm.h"
#include "esp32-hal-cpu.h"
#endif

#ifdef ESP32
#define INTERRUPT_PIN 13
#else
#define INTERRUPT_PIN 2
#endif

#ifdef DEBUG_DISPLAY
//OLED pins
#define OLED_SDA 4
#define OLED_SCL 15 
#define OLED_RST 16
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

SSD1306Wire display(0x3c, OLED_SDA, OLED_SCL, GEOMETRY_128_64);
#endif

#ifdef ESP32
#define MOTOR_PIN 12
#define MOTOR_PWM_CHANNEL 0
#else
#define MOTOR_PIN 9
#endif

#define BUTTON_PIN 0

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

MPU6050 mpu(0x68);


volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high

#ifdef ESP32
void IRAM_ATTR dmpDataReady() {
#else
void dmpDataReady() {
#endif 

  mpuInterrupt = true;
}


void displayDebug(const String& message) {
  #ifdef DEBUG_DISPLAY
  display.clear();
  display.drawString(0, 0, message);
  display.display();
  #endif
}

RunLengthEncoding rle;

void mpuInitialize(bool full) {
  Wire.setClock(400000);
  mpu.initialize();
  pinMode(INTERRUPT_PIN, INPUT);

  devStatus = mpu.dmpInitialize();

  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788);

  if (devStatus == 0) {
    // Calibration Time: generate offsets and calibrate our MPU6050
    mpu.CalibrateAccel(6);
    mpu.CalibrateGyro(6);
    mpu.PrintActiveOffsets();
    // turn on the DMP, now that it's ready
    mpu.setDMPEnabled(true);

    // enable Arduino interrupt detection
    #ifdef ESP32
    attachInterrupt(INTERRUPT_PIN, dmpDataReady, RISING);
    #else 
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    #endif
    mpuIntStatus = mpu.getIntStatus();

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    displayDebug("DMP Ready");
    Serial.println("DMP Ready");
    dmpReady = true;
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    displayDebug("Error");
    Serial.print("Erroer: ");
    Serial.println(devStatus);
  }
}

void setup() {
  Serial.begin(115200);

  // Reset OLED
  #ifdef ESP32
  pinMode(OLED_RST, OUTPUT);
  digitalWrite(OLED_RST, LOW);
  delay(20);
  digitalWrite(OLED_RST, HIGH);
  #endif

  pinMode(LED_BUILTIN, OUTPUT);

  /*//initialize OLED
  Wire1.begin(OLED_SDA, OLED_SCL);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  display.setTextColor(WHITE);
  display.setTextSize(1);
  displayDebug("OLED Ready");*/

  #ifdef DEBUG_DISPLAY
  Serial.println("Initializing display");
  display.init();
  display.clear();
  display.setFont(ArialMT_Plain_24);
  display.drawString(0, 0, "OLED Ready");
  display.display();
  #else 
  #ifdef ESP32
  Wire.begin(4, 15);
  #else
  Wire.begin();
  #endif
  #endif
  
  mpuInitialize(true);
  
  // set motor to default speed
  int speed = 200;
  #ifdef ESP32
  ledcSetup(MOTOR_PWM_CHANNEL, 5000/*Hz*/, /*resolution (bit)*/ 8);
  ledcAttachPin(MOTOR_PIN, MOTOR_PWM_CHANNEL);
  ledcWrite(MOTOR_PWM_CHANNEL, speed);

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  #else 
  pinMode(MOTOR_PIN, OUTPUT);
  analogWrite(MOTOR_PIN, speed);
  #endif

  
  
  //esp_bluedroid_disable();
  //esp_bt_controller_disable();
  //esp_wifi_stop();

#ifdef ESP32
  setCpuFrequencyMhz(80);
  Serial.print("CPU Freq: ");
  Serial.println(getCpuFrequencyMhz());
 #endif
}

#define AVERAGE_COUNT 10
int16_t lastValues[AVERAGE_COUNT];
int currentIndex = 0;

void loop() {
  if (!dmpReady) return;

#ifdef ESP32
  if(!digitalRead(BUTTON_PIN)) {
    displayDebug("Reinit");
    mpuInitialize(false);
  }
  #endif

  // wait for MPU interrupt or extra packet(s) available
  while (!mpuInterrupt && fifoCount < packetSize) {
    if (mpuInterrupt && fifoCount < packetSize) {
      // try to get out of the infinite loop
      fifoCount = mpu.getFIFOCount();
    }
    // other program behavior stuff here
    // .
    // if you are really paranoid you can frequently test in between other
    // stuff to see if mpuInterrupt is true, and if so, "break;" from the
    // while() loop to immediately process the MPU data
  }

  // reset interrupt flag and get INT_STATUS byte
  mpuInterrupt = false;
  mpuIntStatus = mpu.getIntStatus();

  // get current FIFO count
  fifoCount = mpu.getFIFOCount();
  if (fifoCount < packetSize) {
    //Lets go back and wait for another interrupt. We shouldn't be here, we got an interrupt from another event
    // This is blocking so don't do it   while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
  }
  // check for overflow (this should never happen unless our code is too inefficient)
  else if ((mpuIntStatus & (0x01 << MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
    // reset so we can continue cleanly
    mpu.resetFIFO();
    //  fifoCount = mpu.getFIFOCount();  // will be zero after reset no need to ask
    //displayDebug("FIFO Overflow");

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
  } else if (mpuIntStatus & (0x01 << MPU6050_INTERRUPT_DMP_INT_BIT)) {

    // read a packet from FIFO
    while (fifoCount >= packetSize) { // Lets catch up to NOW, someone is using the dreaded delay()!
      mpu.getFIFOBytes(fifoBuffer, packetSize);
      // track FIFO count here in case there is > 1 packet available
      // (this lets us immediately read more without waiting for an interrupt)
      fifoCount -= packetSize;
    }
    
    // display real acceleration, adjusted to remove gravity
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetAccel(&aa, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
    Serial.print("areal\t");
    Serial.print(aaReal.x);
    Serial.print("\t");
    Serial.print(aaReal.y);
    Serial.print("\t");
    Serial.println(aaReal.z);
    /*display.clear();
    display.drawString(0, 0, String(aaReal.x));
    display.drawString(0, 10, String(aaReal.y));
    display.drawString(0, 20, String(aaReal.z));
    display.display();*/

    
    rle.add(aaReal.x);
    lastValues[currentIndex] = aaReal.x;
    currentIndex = (currentIndex + 1) % AVERAGE_COUNT;
    int32_t average = 0;
    for(int i = 0; i < AVERAGE_COUNT; i++) {
      average += lastValues[i];
    }
    average /= AVERAGE_COUNT;
   
    
    /*auto str = rle.encodings[rle.encodings.size() -1].strength;*/

    if(average < -800) {
      displayDebug("LINKS");
      digitalWrite(LED_BUILTIN, HIGH); 
    } else if(average > 800) {
      displayDebug("RECHTS");
      digitalWrite(LED_BUILTIN, LOW);
    } else {
      displayDebug("GERADE");
      digitalWrite(LED_BUILTIN, LOW);
    }
    

    // Idee: kalibriere Sensordaten auf langen Geraden (length > 200)
    
    // displayDebug(String(rle.encodings.size()));

    /*for (auto item : rle.encodings) {
      Serial.print(String(item.strength));
      Serial.print(" ");
      Serial.print(String(item.length));
      Serial.print("\t");
    }
    
    Serial.println();//*/
    
    /*Serial.println(aaReal.x);
    Serial.print("\t");
    Serial.print(aaReal.y);
    Serial.print("\t");
    Serial.println(aaReal.z);
    //*/

  }
}
