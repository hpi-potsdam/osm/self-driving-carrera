#include <Arduino.h>
#include <Vector.h>

#define HISTORY_LENGTH 10

struct Encoding {
  int16_t strength;    // encodes the "direction" of the data => you can see if it is right, left or straight
  uint8_t length;
};

class RunLengthEncoding {
private:
  int threshold = 500;

  bool isDifferenceSmallerThanThreshold(int16_t lastStrength, int16_t data);

public:
  Vector<Encoding> encodings;
  Encoding arr[32];

  RunLengthEncoding();
  void add(int16_t data);
  Encoding at(int index);
  uint16_t wherAmI(Vector<int16_t> history, uint16_t hint);

};