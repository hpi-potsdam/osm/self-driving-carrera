#include "RunLengthEncoding.h"
#include <Arduino.h>

bool RunLengthEncoding::isDifferenceSmallerThanThreshold(int16_t lastStrength, int16_t data) {
    return abs(lastStrength - data) < threshold;
}

RunLengthEncoding::RunLengthEncoding() {
    encodings.setStorage(arr);
}

void RunLengthEncoding::add(int16_t data) {
    if (encodings.size() == 0) {
        encodings.push_back(Encoding{data, 1});
        return;
    }

    Encoding lastElement = *(encodings.end());
    if (isDifferenceSmallerThanThreshold(lastElement.strength, data))
    {
        lastElement.length++;
    } else {
        encodings.push_back(Encoding{data, 1});
    }
}

Encoding RunLengthEncoding::at(int index) {
    if (index < 0) {
        return encodings[encodings.size() - index];
    } else {
        return encodings[index % encodings.size()];
    }
}

uint16_t RunLengthEncoding::wherAmI(Vector<int16_t> history, uint16_t hint) {
    int remainingHistoryLength = history.size();
    int counter = hint;
    while (remainingHistoryLength > 0) {
        remainingHistoryLength -= at(counter - 1).length;
        counter--;
    }

    hint = counter;
    int offset = -remainingHistoryLength;
    int minimum = 32767;
    int minimum_section_index = hint;

    for (int i = 0; i < HISTORY_LENGTH; i++) {
        int sum = 0;
        int size = HISTORY_LENGTH;
        int passes = 0;

        while (size > 0) {
            Encoding currentEncoding = at(hint + passes);

            int step = max(currentEncoding.length - offset, size);
            sum += step * currentEncoding.strength;
            offset = 0;
            size -= step;
            passes++;
        }

        minimum = min(minimum, sum);
        minimum_section_index = hint + passes;
    }

    return minimum_section_index;
}
