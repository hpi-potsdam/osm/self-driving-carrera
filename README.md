# Observing a Moving Target - Reliable Transmission of Debug Logs from Mobile Embedded Devices

This repository contains the source code and documentation (hardware and
software) regarding a [publication](https://arxiv.org/pdf/2110.01412)
which will be published in the context of [1st International Workshop on
Testing Distributed Internet of Things Systems (TDIS)](
https://tdis21.diselab.berlin/), co-located with the [9th IEEE
International Conference on Cloud Engineering (IC2E 2021)](
https://conferences.computer.org/IC2E/2021/).

If you use this in your work, please cite us.
```
@inproceedings{daase_observing_a_moving_target,
  title = {Observing a Moving Target - Reliable Transmission of Debug Logs from Mobile Embedded Devices},
  booktitle = {2021 IEEE International Conference on Cloud Engineering (IC2E) Workshops},
  organization={IEEE},
  year={2021},
  pages={224-230}
}
```

Apart from the collaborative publication, code and documentation in
this repository have been authored by Björn Daase and Leon Matthes.

## Contact

Unfortunately, the git history had to be pruned due to privacy concerns (code and documentation has previously been kept in a repository with related projects by other authors).
If you have questions – whether they can answered by the git log or not – feel free to ask.

## Table of Content

Directory | Description
----------|------------
[doc/](https://gitlab.com/hpi-potsdam/osm/self-driving-carrera/-/tree/main/doc) | This paper, internal and external design documents, and design source files
[plot-data/](https://gitlab.com/hpi-potsdam/osm/self-driving-carrera/-/tree/main/plot-data) | Scripts to visualize slot-car debugging data
[self-driving/](https://gitlab.com/hpi-potsdam/osm/self-driving-carrera/-/tree/main/self-driving) | Source code for the self-driving slot car
[sensor-tests/](https://gitlab.com/hpi-potsdam/osm/self-driving-carrera/-/tree/main/sensor-tests) | Source code for miscellaneous ESP32 sensor tests

## License
```
# SPDX-License-Identifier: GPL-2.0
```
