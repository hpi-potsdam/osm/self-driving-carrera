import matplotlib.pyplot as plt
import numpy as np
import csv
import sys

x=[]
y_vin=[]
y_capacitor=[]


if(len(sys.argv) < 2):
    print('Please provide path to .tsv file')
    exit(1)


with open(sys.argv[1], 'r') as csvfile:
    plots= csv.reader(csvfile, delimiter='\t')
    next(plots)
    for idx, row in enumerate(plots):
        if (idx < 1000):
            continue
        x.append(((idx - 1000) * 150) / 1000)
        y_vin.append(float(row[0]))
        y_capacitor.append(float(row[1]))


plt.plot(x,y_vin, color="#0080FF", label="Input")
plt.plot(x,y_capacitor, color="#303030", label="Capacitor")

plt.xlabel('Time [ms]', size=18)
plt.ylabel('Voltage [V]',  size=18)

plt.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, 1.2), ncol=2)

plt.yticks(np.arange(0, 10, step=1), size=18)
plt.xticks(size=18)

plt.savefig(sys.argv[1] + '.png', bbox_inches='tight')
plt.show()

