#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if(len(sys.argv) < 3):
    print('Please provide path to .tsv file')
    exit(1)

data1 = pd.DataFrame(pd.read_csv(sys.argv[1], '\t'))
data2 = pd.DataFrame(pd.read_csv(sys.argv[2], '\t'))


list_of_lists1 = (np.array([np.repeat(data[0], data[1]) for i, data in data1.iterrows()]))
flattened1 = [val for sublist in list_of_lists1 for val in sublist]

list_of_lists2 = (np.array([np.repeat(data[0], data[1]) for i, data in data2.iterrows()]))
flattened2 = [val for sublist in list_of_lists2 for val in sublist]

plt.plot(flattened1)
plt.plot(flattened2)
plt.show()
