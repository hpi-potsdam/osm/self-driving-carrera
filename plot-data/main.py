#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd
import sys

if(len(sys.argv) < 2):
    print('Please provide path to .tsv file')
    exit(1)

data = pd.read_csv(sys.argv[1], '\t')
data.plot()
plt.show()
