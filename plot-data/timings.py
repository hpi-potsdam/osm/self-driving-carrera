#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import csv

if(len(sys.argv) < 2):
    print('Please provide path to .tsv file')
    exit(1)

sum = 0
file = open(sys.argv[1], "rU")
reader = csv.reader(file, delimiter='\t')
next(reader)

xtimings = [[],[],[],[],[]]
timings = [[],[],[],[],[]]
x = 0
for row in reader:
    timings[int(row[0])].append(int(row[1]))
    xtimings[int(row[0])].append(x)
    x += 1

for idx, _ in enumerate(timings):
    plt.scatter(xtimings[idx], timings[idx], label=idx)

plt.show()
        
