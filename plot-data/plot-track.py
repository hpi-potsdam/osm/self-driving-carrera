#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

if(len(sys.argv) < 2):
    print('Please provide path to .tsv file')
    exit(1)

data = pd.DataFrame(pd.read_csv(sys.argv[1], '\t'))


list_of_lists = (np.array([np.repeat(data[0], data[1]) for i, data in data.iterrows()]))
flattened = [val for sublist in list_of_lists for val in sublist]

plt.plot(flattened)
plt.show()
