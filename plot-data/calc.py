#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import csv

if(len(sys.argv) < 2):
    print('Please provide path to .tsv file')
    exit(1)

sum = 0
file = open(sys.argv[1], "rU")
reader = csv.reader(file, delimiter='\t')
next(reader)
for row in reader:
    sum += abs(float(row[0]) * float(row[1]))

print(sum)
        